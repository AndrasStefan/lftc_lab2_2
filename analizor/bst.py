class Node:
    def __init__(self, value, position, left=None, right=None, parent=None):
        self.value = value
        self.position = position
        self.left = left
        self.right = right
        self.parent = parent

    def is_root(self):
        return not self.parent

    def is_leaf(self):
        return not (self.left or self.right)


class BinarySearchTree:
    def __init__(self):
        self.root = None

    def insert(self, value, position=0):
        if self.root:
            self._insert(value, position, self.root)
        else:
            self.root = Node(value, position)

    def _insert(self, value, position, current_node):
        if value < current_node.value:
            if current_node.left:
                self._insert(value, position, current_node.left)
            else:
                current_node.left = Node(value, position, parent=current_node)
        else:
            if current_node.right:
                self._insert(value, position, current_node.right)
            else:
                current_node.right = Node(value, position, parent=current_node)

    def get(self, value):
        if self.root:
            rez = self._get(value, self.root)
            return getattr(rez, 'position', None)
        else:
            return None

    def _get(self, value, current_node):
        if not current_node:
            return None
        elif current_node.value == value:
            return current_node
        elif value < current_node.value:
            return self._get(value, current_node.left)
        else:
            return self._get(value, current_node.right)

    def inorder(self):
        rez = []
        self._inorder(self.root, rez)
        return rez

    def _inorder(self, current_node, ans):
        if not current_node:
            return

        self._inorder(current_node.left, ans)

        ans.append((
            current_node.value,
            current_node.position,
            current_node.left,
            current_node.right,
        ))

        self._inorder(current_node.right, ans)

    @staticmethod
    def test():
        bst = BinarySearchTree()
        nodes = [10, 12, 5, 4, 20, 8, 7, 15, 13]

        for i, node in enumerate(nodes):
            bst.insert(node, i)

        print(bst.inorder())


if __name__ == "__main__":
    BinarySearchTree.test()
