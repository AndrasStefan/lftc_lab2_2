from analizor.scanner import Scanner


def print_color(txt, color='green'):
    text = str(txt)

    colors = {
        'green': '\033[92m',
        'red': '\033[00;31m',
    }

    bold = "\033[1m"
    ENDC = '\033[0m'
    print(colors.get(color) + bold + text + ENDC)


if __name__ == "__main__":
    scanner = Scanner('../prog1.py')
    scanner.start()

    print_color("Program Internal Form")
    scanner.print_fip()

    print_color("Symbol Table - Identifiers")
    scanner.identifier_table.print()

    print_color("Symbol Table - Constants")
    scanner.constant_table.print()

    print_color("Lexical errors: {}".format(len(scanner.errors)), color='red')
    for error in scanner.errors:
        print_color(error, color='red')
