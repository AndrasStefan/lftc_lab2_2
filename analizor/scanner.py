from prettytable import PrettyTable

from af.automat_finit import AF
from analizor.ts import SymbolTable


class Scanner:
    MAX_IDENTIFIERS_LENGTH = 8
    SEPARATORS = ['\n', ' ', ':', '(', ')']
    OPERATORS = ['+', '-', '/', '*', '=', '!=', '<', '>']
    KEYWORDS = ['input', 'print', 'while', 'if', 'else', 'int', 'float']

    def __init__(self, filename, table='table.txt'):
        self.identifier_table = SymbolTable()
        self.constant_table = SymbolTable()
        self.fip = []
        self.errors = []

        self.filename = filename
        self.table_filename = table

        self.tokens = self._tokenize()
        self.table = self._parse_table()

        self.af_constants = AF()
        self.af_constants.read_from_file('../af/constants.txt')

        self.af_identifier = AF()
        self.af_identifier.read_from_file('../af/identifiers.txt')

    def _parse_table(self):
        with open(self.table_filename, 'rb') as file:
            return {line.split()[0].decode(
                'unicode-escape'): line.split()[1].decode(
                'unicode-escape') for line in file}

    def _tokenize(self):
        rez = []

        with open(self.filename) as file:
            for line_number, line in enumerate(file):
                rez.extend(self._get_atoms_from_line(line, line_number))

        return rez

    def _get_atoms_from_line(self, line, line_number):
        i, atoms = 0, []

        while i < len(line):
            atom = ''

            while line[i] not in self.SEPARATORS + self.OPERATORS:
                atom += line[i]
                i += 1
                if i == len(line):
                    break

            atom and atoms.append((atom, line_number))

            if i < len(line) and line[i] in self.SEPARATORS + self.OPERATORS and line[i] != ' ':
                if line[i] == '=' and atoms[-1][0] == '!':
                    atoms[-1] = ('!=', line_number)
                else:
                    atoms.append((line[i], line_number))

            i += 1

        return atoms

    def _is_constant_af(self, atom):
        return self.af_constants.check(atom)

    def _is_identifier_af(self, atom):
        return self.af_identifier.check(atom)

    def start(self):
        for token, line_number in self.tokens:
            if token in self.KEYWORDS + self.SEPARATORS + self.OPERATORS:
                self.fip.append((self.table.get(token), 0, token))

            elif self._is_identifier_af(token) and len(token) <= self.MAX_IDENTIFIERS_LENGTH:
                index = self.identifier_table.get_or_insert(token)
                self.fip.append((self.table.get('identifier'), index, token))

            elif self._is_constant_af(token):
                index = self.constant_table.get_or_insert(token)
                self.fip.append((self.table.get('constant'), index, token))

            else:
                self.errors.append("On line {} !".format(line_number + 1))

    def print_fip(self):
        table = PrettyTable()
        table.field_names = ['Code', 'Index', 'Token']

        for code, index, token in self.fip:
            table.add_row([code, index, repr(token)])

        print(table)
