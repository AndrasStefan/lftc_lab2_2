from prettytable import PrettyTable

from analizor.bst import BinarySearchTree


class SymbolTable:
    def __init__(self):
        self.bts = BinarySearchTree()
        self.length = 0

    def _insert(self, value):
        self.length += 1
        self.bts.insert(value, self.length)

    def get_or_insert(self, value):
        position = self.bts.get(value)

        if position:
            return position
        else:
            self._insert(value)
            return self.length

    def print(self):
        table = PrettyTable()
        table.field_names = ['Index', 'Token', 'Left', 'Right']

        for token, index, left, right in sorted(self.bts.inorder(), key=lambda elem: elem[1]):
            table.add_row([index, token, getattr(left, 'position', None), getattr(right, 'position', None)])

        print(table)
