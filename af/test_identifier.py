import string

from af.automat_finit import AF

af = AF()
af.read_from_file('identifiers.txt')

for letter in string.ascii_letters:
    assert af.check(letter)

good = ['abcABC', 'ab123', 'aB123Abc', 'qweasdqweQ', 'numevariabila1', 'numevariabila2']
bad = ['2ac', '123', 'qweasd-Q', ',qwe213', '-av']

for item in good:
    assert af.check(item)

for item in bad:
    assert not af.check(item)
