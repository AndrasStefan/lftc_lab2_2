from collections import namedtuple

Tranzitie = namedtuple('Tranzitie', 'initial final simbol')


class AF:
    def __init__(self):
        self.stari, self.alfabet, self.stari_finale, self.tranzitii = set(), set(), set(), set()
        self.stare_initiala = None

    def read_from_file(self, filename):
        with open(filename) as f:
            stari = f.readline().split()
            tipuri = f.readline().split()
            self.stari.update(stari)

            for stare, tip in zip(stari, tipuri):
                if tip == '1':
                    self.stare_initiala = stare
                elif tip == '2':
                    self.stari_finale.add(stare)

            for line in f:
                initial, final, simbol_list = line.split()
                for simbol in simbol_list.split(','):
                    self.tranzitii.add(Tranzitie(initial=initial, final=final, simbol=simbol))
                    self.alfabet.add(simbol)

    def _get_next_state(self, initial, simbol):
        return next((
            final
            for init, final, simb in self.tranzitii
            if init == initial and simb == simbol
        ), None)

    def check(self, secventa):
        current = self.stare_initiala

        for simbol in secventa:
            current = self._get_next_state(current, simbol)
            if not current:
                return False

        return current in self.stari_finale

    def cel_mai_lung_prefix(self, secventa):
        prefixe = [secventa[:-i] for i in range(1, len(secventa))]

        return next((
            elem for elem in prefixe if self.check(elem)
        ), '')
