import string

from af.automat_finit import AF

af = AF()
af.read_from_file('constants.txt')

for letter in string.ascii_letters:
    assert not af.check(letter)

good = ['3.14', '0.1', '0.0002', '1.0']
bad = ['3,14', 'ab', '4a', '123.a']

for item in good:
    assert af.check(item)

for item in bad:
    assert not af.check(item)
